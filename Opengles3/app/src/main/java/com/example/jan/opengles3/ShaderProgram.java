package com.example.jan.opengles3;

import android.opengl.GLES30;

/**
 * Created by zhichao on 17/8/13.
 *
 * 二次封装的着色器类ShaderProgram
 * 完成着色器的生成，链接，属性设置等
 */

public class ShaderProgram {
    private int mProgram;

    public ShaderProgram(String vertexShaderCode, String fragmentShaderCode){
        int vertexShader = loadShader(GLES30.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = loadShader(GLES30.GL_FRAGMENT_SHADER, fragmentShaderCode);

        mProgram = GLES30.glCreateProgram();
        GLES30.glAttachShader(mProgram, vertexShader);
        GLES30.glAttachShader(mProgram, fragmentShader);
        GLES30.glLinkProgram(mProgram);
    }

    public int getProgram() {
        return mProgram;
    }

    public int getAttribLocation (String attrib) {
        return GLES30.glGetAttribLocation(mProgram, attrib);
    }

    public int getUniformLocation (String uniform) {
        return GLES30.glGetUniformLocation(mProgram, uniform);
    }

    private int loadShader(int type, String shaderCode) {
        int shader = GLES30.glCreateShader(type);
        GLES30.glShaderSource(shader, shaderCode);
        GLES30.glCompileShader(shader);
        return shader;
    }
}
