package com.example.jan.opengles3;

import android.content.Context;
import java.util.Vector;
import android.opengl.GLES30;

/**
 * Created by zhichao on 17/8/13.
 *
 * 模型类Model
 * 整合Geometry, Material, ShaderProgram等
 * 提供模型的数据解析和绘制的功能等
 */
public class Model {
    private Context mContext;

    private Geometry mGeometry;
    private Material mMaterial;
    private ShaderProgram mShaderProgram;
    private boolean loadComplete;

    public Model(Context context) {
        mContext = context;

        loadComplete = false;
        mGeometry = new Geometry();
        mMaterial = new Material(context);
    }

    public void setupShaderProgram() {
        String vertexShaderCode = "uniform mat4 uMVPMatrix;" +
                "attribute vec4 aPosition;" +
                "attribute vec2 aTexCoord;" +
                "varying vec2 vTexCoord;" +
                "void main() {" +
                "gl_Position = uMVPMatrix * aPosition;" +
                "vTexCoord = aTexCoord; }";
        String fragmentShaderCode = "precision mediump float;" +
                "varying vec2 vTexCoord;" +
                "uniform sampler2D sTexture;" +
                "void main() {" +
                "gl_FragColor = texture2D(sTexture, vTexCoord); }";
        mShaderProgram = new ShaderProgram(vertexShaderCode, fragmentShaderCode);

        loadComplete = true;
    }

    public Geometry getGeometry() {
        return mGeometry;
    }

    // 数据解析，从obj格式文本数据中获取点面等数据
    public void parseObj(byte[] data) {
        String text = new String(data);

        Vector<Float> vertices = new Vector<>();
        Vector<Float> normals = new Vector<>();
        Vector<Float> uvs = new Vector<>();
        Vector<Integer> faces = new Vector<>();
        Vector<Character> vtn = new Vector<>();

        String[] lines = text.split("\n");
        String line = "";
        char lineFirstChar = ' ';
        char lineSecondChar = ' ';
        for (int i = 0; i < lines.length; i++) {
            line = lines[i].trim();
            if (line.length() == 0) continue;
            lineFirstChar = line.charAt(0);
            if (lineFirstChar == '#') continue;

            String[] seg = line.split("\\s+");
            if (seg[0].equals("v")) {
                vertices.addElement(Float.valueOf(seg[1]));
                vertices.addElement(Float.valueOf(seg[2]));
                vertices.addElement(Float.valueOf(seg[3]));
                if (vtn.indexOf('v') == -1) {
                    vtn.addElement('v');
                }
            }
            else if (seg[0].equals("vt")) {
                uvs.addElement(Float.valueOf(seg[1]));
                uvs.addElement(Float.valueOf(seg[2]));
                if (seg.length < 4) {
                    uvs.addElement(0.0f);
                }
                else {
                    uvs.addElement(Float.valueOf(seg[3]));
                }
                if (vtn.indexOf('t') == -1) {
                    vtn.addElement('t');
                }
            } else if (seg[0].equals("vn")) {
                normals.addElement(Float.valueOf(seg[1]));
                normals.addElement(Float.valueOf(seg[2]));
                normals.addElement(Float.valueOf(seg[3]));
                if (vtn.indexOf('n') == -1) {
                    vtn.addElement('n');
                }
            }
            else if (seg[0].equals("f")) {
                for (int j = 1; j <= 3; j++) {
                    String[] seg2 = seg[j].split("/");
                    for (int k = 0; k < seg2.length; k++) {
                        faces.addElement(Integer.valueOf(seg2[k]) - 1);
                    }
                }
            }
        }

        mGeometry.setup(vertices, uvs, normals, faces, vtn);
        //mGeometry.normalize();
        mGeometry.createBuffer();
    }

    /**
     * 加载纹理
     */
    public void loadTexture(String filename) {
        mMaterial.loadTexture(filename);
    }

    /**
     * 绘制模型
     * @param matrix 矩阵
     */
    public void draw(float[] matrix) {
        if (!loadComplete) return;
        GLES30.glUseProgram(mShaderProgram.getProgram());

        GLES30.glUniformMatrix4fv(mShaderProgram.getUniformLocation("uMVPMatrix"), 1, false, matrix, 0);

        GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, mMaterial.getTexture());
        GLES30.glVertexAttribPointer(mShaderProgram.getAttribLocation("aPosition"), 3,
                GLES30.GL_FLOAT, false, 12, mGeometry.getVertexBuffer());
        GLES30.glVertexAttribPointer(mShaderProgram.getAttribLocation("aTexCoord"), 2,
                GLES30.GL_FLOAT, false, 8, mGeometry.getTextureBuffer());
        GLES30.glEnableVertexAttribArray(mShaderProgram.getAttribLocation("aPosition"));
        GLES30.glEnableVertexAttribArray(mShaderProgram.getAttribLocation(("aTexCoord")));
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, mGeometry.getTriangleNum() * 3,
                GLES30.GL_UNSIGNED_SHORT, mGeometry.getTriangleBuffer());
    }
}
