package com.example.jan.opengles3;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Vector;

/**
 * Created by zhichao on 17/8/12.
 *
 * 网格类Geometry
 * 管理网格的数据（顶点位置，UV，法线，三角面片）
 * 将顶点数据写入缓存
 */

public class Geometry {
    private int mVertexNum;
    private int mTriangleNum;

    private float[] mVertices;
    private float[] mNormals;
    private float[] mTextures;
    private short[] mTriangles;

    private boolean useNormal;
    private boolean useUv;

    private float midX, midY, midZ;
    private float disX, disY, disZ;

    private FloatBuffer mVertexBuffer;
    private FloatBuffer mNormalBuffer;
    private FloatBuffer mTextureBuffer;
    private ShortBuffer mTriangleBuffer;

    public Geometry() {
        useNormal = false;
        useUv = false;
    }

    // 构建可用于渲染的网格数据
    public void setup(Vector<Float> vertices, Vector<Float> uvs, Vector<Float> normals,
                       Vector<Integer> faces, Vector<Character> vtn) {
        int count = vtn.size();
        if (vtn.indexOf('n') != -1) {
            useNormal = true;
        }
        if (vtn.indexOf('t') != -1) {
            useUv = true;
        }

        mTriangleNum = faces.size() / (3 * count);
        mVertexNum = mTriangleNum * 3;

        mVertices = new float[mVertexNum * 3];
        if (useNormal) {
            mNormals = new float[mVertexNum * 3];
        }
        if (useUv) {
            mTextures = new float[mVertexNum * 3];
        }
        mTriangles = new short[mTriangleNum * 3];

        for (int i = 0; i < mVertexNum; i++) {
            int vertexIndex = faces.elementAt(count * i);
            mVertices[3 * i] = vertices.elementAt(3 * vertexIndex);
            mVertices[3 * i + 1] = vertices.elementAt(3 * vertexIndex + 1);
            mVertices[3 * i + 2] = vertices.elementAt(3 * vertexIndex + 2);

            if (useUv) {
                int uvIndex = faces.elementAt(count * i + 1);
                mTextures[2 * i] = uvs.elementAt(3 * uvIndex);
                mTextures[2 * i + 1] = 1 - uvs.elementAt(3 * uvIndex + 1);
            }

            if (useNormal) {
                int offset = 2;
                if (!useUv) offset = 1;
                int normalIndex = faces.elementAt(count * i + offset);
                mNormals[3 * i] = normals.elementAt(3 * normalIndex);
                mNormals[3 * i + 1] = normals.elementAt(3 * normalIndex + 1);
                mNormals[3 * i + 2] = normals.elementAt(3 * normalIndex + 2);
            }

            mTriangles[i] = (short)i;
        }

        float minX = 0.0f, maxX = 0.0f, minY = 0.0f, maxY = 0.0f, minZ = 0.0f, maxZ = 0.0f;
        for (int i = 0; i < mVertices.length / 3; i++) {
            if (mVertices[3 * i] < minX) {
                minX = mVertices[3 * i];
            }
            if (mVertices[3 * i] > maxX) {
                maxX = mVertices[3 * i];
            }
            if (mVertices[3 * i + 1] < minY) {
                minY = mVertices[3 * i + 1];
            }
            if (mVertices[3 * i + 1] > maxY) {
                maxY = mVertices[3 * i + 1];
            }
            if (mVertices[3 * i + 2] < minZ) {
                minZ = mVertices[3 * i + 2];
            }
            if (mVertices[3 * i + 2] > maxZ) {
                maxZ = mVertices[3 * i + 2];
            }
        }

        disX = maxX - minX;
        disY = maxY - minY;
        disZ = maxZ - minZ;
        midX = (minX + maxX) / 2.0f;
        midY = (minY + maxY) / 2.0f;
        midZ = (minZ + maxZ) / 2.0f;
    }

    // 网格归一化
    public void normalize() {
        float dis = disX;
        if (disY > dis) {
            dis = disY;
        }
        if (disZ > dis) {
            dis = disZ;
        }

        for (int i = 0; i < mVertices.length / 3; i++) {
            mVertices[3 * i] = (mVertices[3 * i] - midX) / dis;
            mVertices[3 * i + 1] = (mVertices[3 * i + 1] - midY) / dis;
            mVertices[3 * i + 2] = (mVertices[3 * i + 2] - midZ) / dis;
        }
    }

    /**
     * 创建缓存
     */
    public void createBuffer() {
        // 点存入buffer
        ByteBuffer verBuf = ByteBuffer.allocateDirect(4 * mVertices.length); // float占4个字节
        verBuf.order(ByteOrder.nativeOrder());
        mVertexBuffer = verBuf.asFloatBuffer();
        mVertexBuffer.put(mVertices);
        mVertexBuffer.position(0);

        // 法线存入buffer
        if (useNormal) {
            ByteBuffer norBuf = ByteBuffer.allocateDirect(4 * mNormals.length);
            norBuf.order(ByteOrder.nativeOrder());
            mNormalBuffer = norBuf.asFloatBuffer();
            mNormalBuffer.put(mNormals);
            mNormalBuffer.position(0);
        }

        // 纹理坐标存入buffer
        ByteBuffer texBuf = ByteBuffer.allocateDirect(4 * mTextures.length);
        texBuf.order(ByteOrder.nativeOrder());
        mTextureBuffer = texBuf.asFloatBuffer();
        mTextureBuffer.put(mTextures);
        mTextureBuffer.position(0);

        // 面存入buffer
        ByteBuffer triBuf = ByteBuffer.allocateDirect(2 * mTriangles.length);
        triBuf.order(ByteOrder.nativeOrder());
        mTriangleBuffer = triBuf.asShortBuffer();
        mTriangleBuffer.put(mTriangles);
        mTriangleBuffer.position(0);
    }

    public FloatBuffer getVertexBuffer() {
        return mVertexBuffer;
    }

    public FloatBuffer getNormalBuffer() {
        return mNormalBuffer;
    }

    public FloatBuffer getTextureBuffer() {
        return mTextureBuffer;
    }

    public ShortBuffer getTriangleBuffer() {
        return mTriangleBuffer;
    }

    public int getTriangleNum() {
        return mTriangleNum;
    }

    public float[] getMid() {
        float[] mid = {midX, midY, midZ};
        return mid;
    }

    public float getMaxDis() {
        float dis = disX;
        if (disY > dis) {
            dis = disY;
        }
        if (disZ > dis) {
            dis = disZ;
        }

        return dis;
    }
}
