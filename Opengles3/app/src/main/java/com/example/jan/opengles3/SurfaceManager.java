package com.example.jan.opengles3;

import android.content.Context;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.Matrix;
import android.view.MotionEvent;
import java.util.HashMap;
import java.util.Map;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * 渲染显示窗口管理类SurfaceManager
 * 提供了OpenGL的渲染通道
 * 在这个类中设置渲染的基本设置，包括：投影、视图模型矩阵、灯光等
 * 目前所有需要渲染的模型都存储在一个Map中，通过Map来进行模型的添加删除管理
 */
public class SurfaceManager extends GLSurfaceView implements Renderer {

    private final float SCALE = 0.4f; // 防止变化过快
    private final float[] mProjMatrix = new float[16];
    private final float[] mVMatrix = new float[16];
    private final float[] mMVPMatrix = new float[16];

    private Context mContext;

    private Map<String, Model> mModels; // 使用一个Map来存储不同道具的model类对象
    private float mAngle; // 水平旋转角度
    private float mPreviousX; // 触发down事件时的坐标
    private float mPreviousY;
    private boolean mIsScale = false; // 是否是缩放状态
    private boolean mIsMove = false; // 是否是移动状态
    private boolean mIsRatation = false; // 是否是旋转状态

    public SurfaceManager(Context context) {
        super(context);
        mContext = context;
        setEGLContextClientVersion(3);

        setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        setRenderer(this); // 设置renderer
        requestFocus(); // 获取焦点
        setFocusableInTouchMode(true); // 设置为可触控

        mAngle = 0;
        mModels = new HashMap<>() ;
    }

    // 添加道具，网格数据（byte[]）直接传入
    public void applyProp(final String propName, final byte[] meshData, final String textureFilename) {
        queueEvent(new Runnable() {
            @Override
            public void run() {
                Model model = new Model(mContext);
                model.parseObj(meshData);
                model.loadTexture(textureFilename);
                model.setupShaderProgram();

                mModels.put(propName, model);
            }
        });
    }

    // 添加道具，传入模型文件和纹理贴图的文件名称
    public void applyProp(final String propName, final String objFilename, final String textureFilename) {
        queueEvent(new Runnable() {
            @Override
            public void run() {
                try {
                    Model model = new Model(mContext);
                    model.parseObj(FileLoader.readStream(mContext.getResources().getAssets().open(objFilename)));
                    model.loadTexture(textureFilename);
                    model.setupShaderProgram();

                    mModels.put(propName, model);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES30.glClearColor(1.f, 1.f, 1.f, 1.f);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES30.glViewport(0, 0, width, height);

        float ratio = (float) width / height;
        float zoom = 1.8f;
        Matrix.frustumM(mProjMatrix, 0, -ratio / zoom, ratio / zoom, -1 / zoom, 1 / zoom, 1, 5000);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

        GLES30.glEnable(GLES30.GL_DEPTH_TEST); //  开启深度测试
        GLES30.glDisable(GLES30.GL_CULL_FACE); // 关闭背面裁剪

        Matrix.setLookAtM(mVMatrix, 0, 0, 0, 1.5f, 0f, 0f, 0f, 0f, 1.f, 0f);

        float[] mMMatrix = new float[16];
        Matrix.setIdentityM(mMMatrix, 0);

        float scaleFactor = 1.0f / 1739.f;
        Matrix.scaleM(mMMatrix, 0, scaleFactor, scaleFactor, scaleFactor);
        float[] mid = {-0.0026550293f, 869.40393f, 43.511105f};
        Matrix.translateM(mMMatrix, 0, -mid[0], -mid[1], -mid[2]);

        Matrix.rotateM(mMMatrix, 0, mAngle, 0, 1, 0);

        Matrix.multiplyMM(mMVPMatrix, 0, mVMatrix, 0, mMMatrix, 0);
        Matrix.multiplyMM(mMVPMatrix, 0, mProjMatrix, 0, mMVPMatrix, 0);

        // 渲染不同的模型
        for (String key : mModels.keySet()) {
            mModels.get(key).draw(mMVPMatrix);
        }
    }

    /**
     * 点击事件
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int pointCount = event.getPointerCount();
        int action = event.getAction();
        if (pointCount == 1) {
            float x = event.getX();
            float y = event.getY();
            switch (action) {
                case MotionEvent.ACTION_MOVE:
                    float dx = x - mPreviousX;
                    float dy = y - mPreviousY;
                    if (!mIsScale)
                        if (Math.abs(dy) > Math.abs(dx) && !mIsRatation) { // 旋转状态不能移动
                            mIsMove = true;
                        } else if (!mIsMove) { // 移动状态不能旋转
                            mIsRatation = true;
                            mAngle += dx * SCALE;
                        }
                    break;
                case MotionEvent.ACTION_UP:
                    mIsScale = false;
                    mIsMove = false;
                    mIsRatation = false;
                    break;
            }
            mPreviousX = x;
            mPreviousY = y;
        }
        return true;
    }

}
