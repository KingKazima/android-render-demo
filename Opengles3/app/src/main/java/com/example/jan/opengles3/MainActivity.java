package com.example.jan.opengles3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import java.io.InputStream;

/**
 * 测试程序主程序入口MainActivity
 *
 * 测试程序提供了不同道具模型的读取和渲染示例
 * 提供的道具读取和添加函数有两种：一种是内置了读取的过程；另一种将文件的读取过程分离出来
 */
public class MainActivity extends AppCompatActivity {

    private SurfaceManager mSurfaceView;
    private Button frockBtn, shortsBtn, shoesBtn, glassesBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FrameLayout frameLayoutGlView = (FrameLayout)findViewById(R.id.glview);
        mSurfaceView = new SurfaceManager(this);
        frameLayoutGlView.addView(mSurfaceView);

        // 道具读取添加方式一
        frockBtn = (Button)findViewById(R.id.frock);
        frockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSurfaceView.applyProp("frock", "frock.obj", "frock.png");
            }
        });

        shortsBtn = (Button)findViewById(R.id.shorts);
        shortsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSurfaceView.applyProp("shorts", "shorts.obj", "shorts.jpg");
            }
        });

        // 道具读取添加方式二
        shoesBtn = (Button)findViewById(R.id.shoes);
        shoesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    InputStream in = getResources().getAssets().open("shoes.obj");
                    byte[] meshData = FileLoader.readStream(in);
                    mSurfaceView.applyProp("shoes", meshData, "shoes.jpg");
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        glassesBtn = (Button)findViewById(R.id.glasses);
        glassesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    InputStream in = getResources().getAssets().open("glasses.obj");
                    byte[] meshData = FileLoader.readStream(in);
                    mSurfaceView.applyProp("glasses", meshData, "glasses.jpg");
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(mSurfaceView != null) {
            mSurfaceView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mSurfaceView != null) {
            mSurfaceView.onPause();
        }
    }
}
