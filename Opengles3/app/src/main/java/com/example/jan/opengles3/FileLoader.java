package com.example.jan.opengles3;

import android.content.Context;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by zhichao on 17/8/15.
 *
 * 模型文件载入类FileLoader
 * 提供不同格式文件的读取方法
 * 目前只提供了obj文本文件的读取函数
 */

public class FileLoader {
    private Context mContext;

    public FileLoader(Context context) {
        mContext = context;
    }

    // 读取文件获得字节数组
    public static byte[] readStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = -1;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();
        return outSteam.toByteArray();
    }
}
