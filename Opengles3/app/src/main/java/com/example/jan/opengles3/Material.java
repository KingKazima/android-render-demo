package com.example.jan.opengles3;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES30;
import android.opengl.GLUtils;

import java.io.IOException;

/**
 * Created by zhichao on 17/8/13.
 *
 * 材质类Material
 * 管理材质属性，贴图等
 * 目前还没有加入材质属性
 */

public class Material {
    private Context mContext;
    private int mTexture;

    public Material(Context context){
        mContext = context;
        mTexture = -1;
    }

    /**
     * 加载纹理
     */
    public void loadTexture(String filename) {
        try {
            //String filename = "head/texture.jpg";
            Bitmap bitmap = BitmapFactory.decodeStream(mContext.getAssets().open(filename));
            int[] textures = new int[1];
            GLES30.glGenTextures(1, textures, 0);

            mTexture = textures[0];
            GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, mTexture);
            GLUtils.texImage2D(GLES30.GL_TEXTURE_2D, 0, bitmap, 0);
            GLES30.glTexParameterf(GLES30.GL_TEXTURE_2D,
                    GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_LINEAR);
            GLES30.glTexParameterf(GLES30.GL_TEXTURE_2D,
                    GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_LINEAR);

            GLES30.glTexParameterf(GLES30.GL_TEXTURE_2D,
                    GLES30.GL_TEXTURE_WRAP_S, GLES30.GL_CLAMP_TO_EDGE);
            GLES30.glTexParameterf(GLES30.GL_TEXTURE_2D,
                    GLES30.GL_TEXTURE_WRAP_T, GLES30.GL_CLAMP_TO_EDGE);
            GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, 0);
            bitmap.recycle();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getTexture() {
        return mTexture;
    }
}
